# What is Beam ?

Beam is a fogponics grow box for growing herbs and plants using fog to feed the root system.

The grow box integrates a number of connected embedded devices, sensors and actuators. An embeddded Linux single board computer (eg. Raspberry Pi) with an Internet connection provides the communication interface between the user and the devices inside the grow box. 

# What is in this repository ?

This repository contains the code to build, test and deploy the Python service that runs on the Raspberry Pi which serves as a communications endpoint for a Websockets/MQTT frontend application and as RS485 multi-drop communications master controller for the network of devices, sensors and actuators. 

# Features

- Developed in Python 3
- Automated build system using Make, Docker compose, build-essentials and Debian package builder tools 
- Can be deployed and run as a docker container using Minideb:Stretch as a base image or deployed and run natively on Debian ARMhf Raspbian OS as a systemd service
- Python Paho Mqtt provides Mqtt for publish/subscribe message interface
- Python Serial provides interface to RS485 multi-drop communication network
- Mongodb and Python Mongodb used for persisting device and user information

# Host build requirements

- Docker
- Make
- Git

# Target build, deploy and run

## Using Docker in application

Beam can run inside a Docker container. The target host i.e. Raspberry Pi requires Docker. There are 3 Docker containers that interact with each other. A Docker container pascaldevink/rpi-mosquitto provides the Mqtt Mosquitto broker. Docker container andresvidal/rpi3-mongodb3 provides a Mongodb server and the Beam service Docker container.

|---------------------|								|-----------------|									|--------------------|
| Mosquitto Broker |       <------------>		| Beam Service |		<------------->		| Mongodb Server |
|---------------------|								|-----------------|									|--------------------|

## Build, test and deply steps for in-application Docker mode

### Mosquitto broker and Mongodb server containers

See https://github.com/andresvidal/rpi3-mongodb3 for Mongodb container. 
See https://github.com/pascaldevink/rpi-mosquitto for Mosquitto container.

{TODO} Insert exact commands to get to pull, build and run for each.

#### Run Mosquitto broker in detached mode listening on all interfaces, port 8883

``` make run-mosquitto ```

Implicit make recipe ```docker run -d -p 8883:8883 pascaldevink/rpi-mosquitto```

#### Run Mongodb server in detached mode listening on localhost interface, port 27017

``` make run-mongo ```

Implicit make recipe ```docker run -d -p 27017:27017 andresvidal/rpi3-mongodb3```

### Beam service container

#### Build
make docker-build

#### Test
{TODO} make docker-test

#### Run interactively
make docker-run

#### Run detached
make docker-run-daemon




## Using native Debian packaged 

In this mode, the Beam service is packaged and deployed on native Debian OS i.e. directly on the RPi and run in a Python virtual environment. The Mongodb server and the Mosquitto broker can still
be run in Docker containers but then requires Docker to be installed on the Rpi. Alternatively, install **mosquitto** and **mongodb** natively on Debian.

{TODO} Test Debian stretch mosquitto and mongodb packages on Rpi and also provide configuration details here.

## Using Docker for build 

In this mode, Beam can be built, packaged and deployed for Debian. Minideb:Stretch docker image with build tools and Debian helper scripts are used to package Beam for Debian.
See the **debian** directory for details. 

### How it works

Makefile contains all the recipes for building the package. Invoking **make deb** builds the Minideb build environment Docker image with all build tools and then executes **dpkg-buildpackage** via Docker using the build image.
The result is an architecture independent Debian package located under **./debian/dist/**.

### Installation

Install natively using **dpkg -i beam_x.x.x_all.deb**. A Beam systemd service will be enabled at boot.


# Development tips

## Mounting remote directory and edit files on host machine

While working on host machine and testing on target machine e.g. Mac (host) to Rpi (target), use **rysnc** and **sshfs**. 

1) Use rsync to synchronise the root beam directory on the Rpi to the host directory

```rsync -a beam pi@x.x.x.x:/home/pi/Workspace  ```

The above will result in a directory **/home/pi/Workspace/beam** that is identical to the host beam directory.

2) Then mount the remote filesystem so you can continue working on the host machine as if it were the remote machine.

``` sshfs pi@x.x.x.x:/home/pi/Workspace/beam beam ```

NOTE: Make sure to un mount using ``` umount beam ```

## Running docker commands on Rpi without sudo

Docker daemon runs as root user and binds to Unix socket so superuser privileges are required to execute docker commands. To avoid using sudo for docker commands, add the user to the docker group.

1) Add docker group if not already added

``` sudo groupadd docker ```

2) Add $USER or any user to group 

``` sudo gpasswd -a $USER docker ```

3) Sync changes

``` newgrp docker ```

## Transferring docker image from host to target machine

1) Save docker image to tar file on host machine

``` docker save -o /home/me/docker-image-name.tar docker-image-name ```

2) Transfer the file to remote machine using scp

``` scp /home/me/docker-image-name.tar pi@x.x.x.x:/home/pi/ ```

3) On the target machine, load the docker image

``` docker load -i /home/pi/docker-image-name.tar ```


# Issues

# Mqtt user API

		```
			1. Get state of all devices and sensors in the cube

			REQUEST:
				topic = {BEAM_ID}/cube/GET
				payload = {"token":""}

			REPLY:
				topic = {BEAM_ID}/cube/REPLY
				payload = {
					"success": True | False,
					"data": {
						"cube": [{},{},{},...]
						"chamber": [{},{},{},...]
					},
					"error"
				}

			2. Change the lighting inside the cube
			{BEAM_ID}/cube/light/PUT

			REQUEST:
				topic = {BEAM_ID}/cube/light/PUT
				payload = {"token":"", "data":{
					"rgb": ffffff,
					"intensity": 60
				}} 

			REPLY:
				topic = {BEAM_ID}/cube/light/REPLY
				payload = {
					"success": True | False,
					"data": {},
					"error": ""
				}

		*	3. Change the C02 level inside the cube

			REQUEST:
				topic = {BEAM_ID}/cube/gas/PUT
				payload = {
					"token":"",
					"data": {
						"level": x
					}
				}

			REPLY:
				topic = {BEAM_ID}/cube/gas/REPLY
				payload = {
					"success": True | False,
					"data": {},
					"error": ""
				}


			4. Change the temperature inside the cube
			
			REQUEST:
				topic = {BEAM_ID}/cube/temperature/PUT
				payload = {
					"token":"",
					"data": {
						"temperature": 23.5
					}
				}

			REPLY:
				topic = {BEAM_ID}/cube/temperature/REPLY
				payload = {
					"success": True | False,
					"data": {},
					"error": ""
				}

			5. Change the humidity inside one or more of the root chambers inside the cube
			
			REQUEST:
				topic = {BEAM_ID}/cube/humidity/PUT
				payload = {
					"token":"",
					"data": {[
						{
							"level": 1,
							"humidity": 60
						},
						{
							"level": 3,
							"humidity": 50
						}]
					}
				}

			REPLY:
				topic = {BEAM_ID}/cube/humidity/REPLY
				payload = {
					"success": True | False,
					"data": {},
					"error": ""
				}

		*	6. Change the air pressure inside the cube
			
			REQUEST:
				topic = {BEAM_ID}/cube/pressure/PUT
				payload = {
					"token":"",
					"data": {
						"pressure": x kPa
					}
				}

			REPLY:
				topic = {BEAM_ID}/cube/pressure/REPLY
				payload = {
					"success": True | False,
					"data": {},
					"error": ""
				}

		```