.PHONY: beam-buildenv \
	beam-deb-build \
	beam-deb-bash \
	beam-mosquitto-build \
	beam-mosquitto-run \
	beam-hub-test-build \
	beam-hub-test-run \
	beam-sim-build \
	beam-sim-run \
	beam-hub-build \
	beam-hub-bash \
	beam-hub-run \
	beam-hub-save \
	beam-cli-build \
	beam-cli-run \
	pascaldevink-run \
	mongo-run

beam-buildenv:
	docker build --file=./docker/beam_deb_build/Dockerfile --tag=beam-deb-build .

beam-deb-build: beam-buildenv
	if [ ! -d ./debian/dist ]; then \
		mkdir ./debian/dist; \
	fi;
	if [ -e ./debian/dist/*.deb ]; then \
		sudo rm ./debian/dist/*.deb; \
	fi;
	docker run --volume=$$(pwd)/debian/dist:/debuild/debian/dist beam-deb-build dpkg-buildpackage -us -uc -b --changes-option=-udebian/dist/

beam-deb-bash:
	docker run -ti beam-build-env /bin/bash




beam-mosquitto-build:
	docker build --file=./docker/beam_mosquitto/Dockerfile --tag=beam-mosquitto .

beam-mosquitto-run:
	docker run -d -p 8883:8883 --volume=$$(pwd)/mosquitto.conf:/etc/mosquitto/mosquitto.conf beam-mosquitto



# hub test docker image
beam-hub-test-build:
	docker build --file=./docker/beam_test/Dockerfile --tag=beam-test .

beam-hub-test-run:
	docker run -ti --volume=$$(pwd)/tests/settings.json:/beam/settings.json beam-test




# simulator docker image
beam-sim-build:
	docker build --file=./docker/beam_sim/Dockerfile --tag=beam-box-sim .

beam-sim-run:
	docker run -ti --network="host" --volume=$$(pwd)/simulator:/beam beam-box-sim



# hub docker image
beam-hub-build:
	docker build --file=./docker/beam_hub/Dockerfile --tag=beam-hub .

beam-hub-bash:
	docker run -ti beam-hub /bin/bash

beam-hub-run:
	docker run -ti --network="host" --volume=$$(pwd)/beam:/beam/beam beam-hub

beam-hub-save:
	docker save beam-hub | gzip > beam-docker-image.tar.gz



beam-cli-build:
	docker build --file=./docker/beam_cli/Dockerfile --tag=beam-cli .

beam-cli-run:
	docker run -ti --network="host" --volume=$$(pwd)/cli:/beam beam-cli








pascaldevink-run:
	docker run -d -p 8883:8883 pascaldevink/rpi-mosquitto

mongo-run:
	docker run -d -p 27017:27017 andresvidal/rpi3-mongodb3
