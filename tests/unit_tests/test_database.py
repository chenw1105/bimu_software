@pytest.mark.unit
def test_base(env):

	# setup
	print('Environment path = {}'.format(env))
	test_env = load_test_environment(env)
	tnetdatabase.test_setup(db_path=test_env['db_path'])
	# run tests
	api_dev_info_get()
	api_dev_info_set()
	api_user_register()
	# teardown
	tnetdatabase.test_teardown()
