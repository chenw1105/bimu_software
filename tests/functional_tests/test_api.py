import json
import time
import random
import sys
import signal
import paho.mqtt.client as mqtt

apis = []

class Api():
    def __init__(self, send_topic, send_payload, recv_topic, recv_payload):
        self._send_topic = send_topic
        self._send_payload = send_payload
        self._recv_topic = recv_topic
        self._exp_recv_payload = recv_payload
        self._result = False

    @property
    def send_topic(self):
        return self._send_topic

    @property
    def send_payload(self):
        return self._send_payload

    @property
    def recv_topic(self):
        return self._recv_topic

    @property
    def result(self):
        return self._result

    def response(self, payload):
        #if payload == self._exp_recv_payload:
        self._result = True

class ScanApi(Api):

    def response(self, payload):
        assert payload == {}
        self._result = True

class RegisterApi(Api):

    def response(self, payload):
        assert 'success' in payload
        assert 'data' in payload
        assert 'error' in payload

        assert payload['success'] == True or payload['success'] == False
        if payload['success']:
            assert payload['error'] == ''
        if not payload['success']:
            assert len(payload['error']) != 0




def callback(topic, payload):
    global apis

    for api in apis:
        if topic == api.recv_topic:
            api.response(payload)

def test_base(messenger):
    global apis

    # make api list

    # scan to find beam
    apis.append(Api('SCAN', {}, 'SCAN READ', {}))

    # register a user
    apis.append(Api(
        # send topic
        '{}/register/POST'.format(),

        # send payload
        {'firstName': 'Clive',
        'lastName': 'Palmer',
        'password': 'mole',
        'email': "clive@makeauspoo.com",
        'deviceId': 'B5697ACE-B353-432B-9ED0-676657ACEB39'},

        # expected return topic
        '{}/register/REPLY'.format(),

        # expected return payload
        {"success": True,
        "data": {
        "id": 1,
        "firstName": "Clive",
        "lastName": "Palmer",
        "isAdmin": True,
        "token": "",
        "settings": {},
        "deviceId": "B5697ACE-B353-432B-9ED0-676657ACEB39"},
        'error': ''}))

    # authenticate a user
	apis.append(Api('{}/auth/GET'.format(settings()['uuid'])), {
		'username': 'clive@makeauspoo.com',
		'password': 'mole',
		'deviceId': 'B5697ACE-B353-432B-9ED0-676657ACEB39'},
		'{}/auth/REPLY'.format(settings()['beam']),
		{'success': True,
		'data':{'token': '',
				'userId':''},
		'error':''})

    # provision one or more modular root chamber devices
    apis.append(Api('{}/devices/provision/POST'.format(settings()['uuid'])), {
        'token': ,
		'userId': ,
        'devices': [{
            'uuid':'1111111111111111',
            'name':'Chamber 1',
            'description':'Root chamber at level 1',
			'chamber': True},{
            'uuid':'2222222222222222',
            'name':'Chamber 2',
            'description':'Root chamber at level 2',
			'chamber': True},{
            'uuid':'3333333333333333',
            'name':'Chamber 3',
            'description':'Root chamber at level 3',
			'chamber': True}]},
		'{}/devices/provision/REPLY'.format(settings()['beam']),
		{'success': True,
		'data':{},
		'error':''})

    # set device attributes

    # set a callback for received messages
    messenger.set_callback(callback)

    # invoke api and test response
    for api in apis:
        messenger.publish(api.send_topic, api.send_payload)
        time.sleep(2)
        assert api.result == True
