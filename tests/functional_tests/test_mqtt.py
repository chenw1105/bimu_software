import paho.mqtt.client as mqtt
import json
import time

mqtt_client = None
settings = None
connected = False

def mqtt_connected(client, userdata, flags, rc):
    global connected
    print('Mqtt connected to broker client={}, userdata={}, flags={}, rc={}'.format(client, userdata, flags, rc))
    connected = True

def mqtt_disconnected(client, userdata, rc):
    global connected
    print('Mqtt disconnected from broker client={}, userdata={}, rc={}'.format(client, userdata, rc))
    connected = False

def mqtt_message_sent(client, userdata, mid):
    print('Mqtt disconnected from broker client={}, userdata={}, mid={}'.format(client, userdata, mid))

def mqtt_message_received(client, userdata, msg):
    print('Mqtt disconnected from broker client={}, userdata={}, msg={}'.format(client, userdata, msg))

def mqtt_config():
    global settings
    with open('/beam/settings.json', 'r') as f:
        settings = json.load(f)

    assert 'mqtt' in settings
    assert 'broker' in settings['mqtt']
    assert 'host' in settings['mqtt']['broker']
    assert 'port' in settings['mqtt']['broker']

    print('Mqtt broker host {}:{}'.format(settings['mqtt']['broker']['host'], settings['mqtt']['broker']['port']))

def mqtt_connect():
    global connected
    global settings
    global mqtt_client

    mqtt_client = mqtt.Client()
    assert mqtt_client is not None

    mqtt_client.on_connect = mqtt_connected
    mqtt_client.on_disconnect = mqtt_disconnected
    mqtt_client.on_publish = mqtt_message_sent
    mqtt_client.on_message = mqtt_message_received

    mqtt_client.connect(settings['mqtt']['broker']['host'], settings['mqtt']['broker']['port'])

    mqtt_client.loop_start()
    connection_time = time.time()
    time.sleep(2)
    if not connected:
        raise TimeoutError

    mqtt_client.disconnect()
    time.sleep(2)

    if connected:
        raise TimeoutError




#mqtt_client.connect(settings['mqtt']['broker']['host'], settings['mqtt']['broker']['port'])
#mqtt_client.loop
