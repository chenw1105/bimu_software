import time
import json
import pytest
import paho.mqtt.client as mqtt

settings = None

def get_settings():
    global settings

    with open('/beam/settings.json', 'r') as f:
        settings = json.load(f)

    assert 'mqtt' in settings
    assert 'uuid' in settings
    assert 'broker' in settings['mqtt']
    assert 'host' in settings['mqtt']['broker']
    assert 'port' in settings['mqtt']['broker']
    assert 'client' in settings['mqtt']
    assert 'id' in settings['mqtt']['client']
    #assert 'subscriptions' in settings['mqtt']['client']

class Messenger(object):

    def __init__(self):
        global settings
        self._callback = None
        self._mqtt = mqtt.Client(settings['mqtt']['client']['id'])
        self._mqtt.on_disconnect = self.disconnected
        self._mqtt.on_connect = self.connected
        self._mqtt.on_publish = self.message_sent
        self._mqtt.on_message = self.message_received

    def set_callback(self, callback):
        self._callback = callback

    def start(self):
        global settings
        self._mqtt.loop_start()
        self._mqtt.connect(settings['mqtt']['broker']['host'], settings['mqtt']['broker']['port'])

    def stop(self):
        self._mqtt.disconnect()
        self._mqtt.loop_stop()

    def connected(self, client, userdata, flags, rc):
        print('Connected to mqtt broker')

        # always subscribe to this one
        self._mqtt.subscribe("SCAN READ")

        # subscribe to others
        #for subscription in settings['mqtt']['client']['subscriptions']:
        #    self._mqtt.subscribe("{}/{}/REPLY".format(settings['uuid'], subscription))

    def publish(self, topic, payload):
        print('Publish topic={} ...'.format(topic))
        self._mqtt.publish(topic, json.dumps(payload))

    def disconnected(self, client, userdata, rc):
        print('Disconnected from mqtt broker')

    def message_received(self, client, userdata, msg):
        print('Received mqtt msg, topic={}, payload={}'.format(msg.topic, msg.payload))

        if self._callback:
            self._callback(msg.topic, json.loads(msg.payload))

    def message_sent(self, client, userdata, mid):
        print("Published mqtt msg: Mid = {}".format(mid))

@pytest.fixture(scope='module')
def messenger():
    get_settings()
    messenger = Messenger()
    messenger.start()
    # wait here so connection to broker can be made before testing apis
    time.sleep(2)
    yield messenger
    messenger.stop()
