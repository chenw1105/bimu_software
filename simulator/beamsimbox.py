import random
import time
import signal
import threading
import sys
import logging
import socket
import struct
import binascii
import collections

DEVICE_TYPE_BASE = 0
DEVICE_TYPE_CHAMBER = 1

REQ_ADVERTISE_TO_MASTER = 0
REQ_REGISTER_DEVICE = 1
REQ_POLL_DEVICE = 2
REQ_CONTROL_DEVICE = 3

REQ_MIN = REQ_ADVERTISE_TO_MASTER
REQ_MAX = REQ_CONTROL_DEVICE

REQ_HEADER_LENGTH = 21
RSP_HEADER_LENGTH = 22

BEAM_ATTRIBUTES = [
	'fogger',
	'humidity',
	'temperature',
	'fan',
	'cooler_pump',
	'cooler_fan',
	'peltier_duty',
	'peltier_polarity',
	'led_colour',
	'led_power',
	'water_empty',
	'chamber_overflow'
	'chamber_pump']


NULL_ADDR = b'0000000000000000'

BeamRequest = collections.namedtuple('BeamRequest', 'cmd, uuid, plen, payload')
BeamResponse = collections.namedtuple('BeamResponse', 'cmd, uuid, result, plen, payload')
BeamFmap = collections.namedtuple('BeamFmap', 'uuid, cmd')

devices = []
beam_function_map = None

def make_bin_response(beam_response):

	if beam_response.plen:
		message = struct.pack('<B16sBH{}s'.format(beam_response.plen), beam_response.cmd, beam_response.uuid, beam_response.result, beam_response.plen, beam_response.payload)
		crc16 = binascii.crc_hqx(message, 0)
		return struct.pack('<B16sBH{}sH'.format(beam_response.plen), beam_response.cmd, beam_response.uuid, beam_response.result, beam_response.plen, beam_response.payload, crc16)
	else:
		message = struct.pack('<B16sBH', beam_response.cmd, beam_response.uuid, beam_response.result, 0x0000)
		crc16 = binascii.crc_hqx(message, 0)
		return struct.pack('<B16sBHH'.format(beam_response.plen), beam_response.cmd, beam_response.uuid, beam_response.result, 0x0000, crc16)

def parse_bin_request(frame):

	data_length = len(frame)
	logging.debug('Received {} bytes'.format(data_length))

	# Is there enough to parse header
	if data_length < REQ_HEADER_LENGTH:
		logging.info("Request header length invalid, got {}".format(data_length))
		return None

	# checksum calculation and comparison
	plen = data_length - REQ_HEADER_LENGTH

	if plen != 0:
		data = struct.unpack('<B16sH{}BH'.format(plen), frame)
		claim = struct.pack('<B16sH{}B'.format(plen), data[0], data[1], data[2], data[3])
		crc16 = binascii.crc_hqx(claim, 0)
		if crc16 != data[4]:
			logging.info("Crc mismatch, got {} but calculated {}".format(data[4], crc16))
			frame = ''
			return None

		return BeamRequest(cmd=data[0], uuid=data[1], plen=data[2], payload=data[3])
	else:
		data = struct.unpack('<B16sHH', frame)
		claim = struct.pack('<B16sH'.format(plen), data[0], data[1], data[2])
		crc16 = binascii.crc_hqx(claim, 0)
		if crc16 != data[3]:
			logging.info("Crc mismatch, got {} but calculated {}".format(data[3], crc16))
			frame = ''
			return None

		return BeamRequest(cmd=data[0], uuid=data[1], plen=0, payload=None)

class Device():
	def __init__(self, server, uuid, name, device_type, attributes):
		self._server = server
		self._uuid = uuid
		self._name = name
		self._device_type = device_type
		self._registered = False
		self._attributes = attributes
		self._thread_stop = False
		self._thread = threading.Thread(target=self.run, daemon=True)

	@property
	def uuid(self):
		return self._uuid

	@property
	def registered(self):
		return self._registered

	def start(self, registered):
		self._registered = registered
		self._thread.start()

	def stop(self):
		if not self._thread_stop:
			return
		self._thread_stop = True
		self._thread.join(timeout=1)

	def scan(self, beam_request):
		return BeamResponse(cmd=beam_request.cmd, uuid=self.uuid, result=0, plen=1, payload=DEVICE_TYPE_BASE)

	def control(self, beam_request):
		data = struct.unpack('<Bi'.format(beam_request.plen), beam_request.payload)

		# attribute valid ?
		if data[0] > len(BEAM_ATTRIBUTES):
			logging.warning('Unknown device attribute {}'.format(data[0]))
			return BeamResponse(cmd=beam_request.cmd, uuid=beam_request.uuid, result=1, plen=0, payload=None)

		logging.info('Setting device {} {} = {}'.format(self._name, BEAM_ATTRIBUTES[data[0]], data[1]))
		self._attributes[data[0]]['val'] = data[1]
		return BeamResponse(cmd=beam_request.cmd, uuid=beam_request.uuid, result=0, plen=0, payload=None)

	def poll(self, beam_request):

		payload = b''
		for attr in self._attributes:
			payload += struct.pack('<Bi', attr['id'], int(attr['val'] * attr['mult']/attr['div']))

		return BeamResponse(cmd=beam_request.cmd, uuid=beam_request.uuid, result=0, plen=len(payload), payload=payload)

	def advertise(self):
		''' Device initiating a message with an advertisement
			BUT must be packed as a response so master can properly unpack it '''

		payload = b''
		payload = struct.pack('<B', self._device_type)
		logging.debug('Device {} advertising ...'.format(self._uuid))
		request = BeamResponse(cmd=REQ_ADVERTISE_TO_MASTER, uuid=self._uuid, result=0, plen=len(payload), payload=payload)
		self._server.device_request(request)

	def register(self, beam_request):

		self._registered = True
		payload = b''
		payload = struct.pack('<B', self._device_type)
		logging.debug('Device {} registering ...'.format(self._uuid))
		return BeamResponse(cmd=beam_request.cmd, uuid=beam_request.uuid, result=0, plen=len(payload), payload=payload)

	def run(self):

		logging.info('Starting {} device simulator'.format(self._name))

		while True:

			if self._thread_stop:
				break

			if not self._registered:
				self.advertise()
				time.sleep(5)
				continue

			for a in self._attributes:
				if a['rand']['type'] is not None:
					if time.time() - a['rand']['last'] > a['rand']['diff']:
						a['rand']['last'] = time.time()
						if a['rand']['type'] == 'uniform':
							a['val'] = random.uniform(a['min'], a['max'])
							logging.debug('Set device {} {} = {:2.1f} {}'.format(self._name, a['name'], a['val'], a['uom']))
						elif a['rand']['type'] == 'choice':
							a['val'] = random.choice([a['min'], a['max']])
							logging.debug('Set device {} {} = {} {}'.format(self._name, a['name'], a['val'], a['uom']))
						elif a['rand']['type'] == 'randrange':
							a['val'] = random.randrange(a['min'], a['max'])
							logging.debug('Set device {} {} = {} {}'.format(self._name, a['name'], a['val'], a['uom']))

			time.sleep(0.5)

		logging.info('Stopping {} device simulator'.format(self._name))

class BeamServer():

	''' Server also acts as the connection handler since only
		one connection can be made at a time '''

	def __init__(self):
		self._thread_stop = False
		self._thread = threading.Thread(target=self.run, daemon=True)
		self._socket = None

	def start(self):
		self._thread.start()

	def stop(self):
		self._thread_stop = True
		self._thread.join(timeout=5)

	def device_request(self, request):
		''' Called by device for advertising '''
		packed_binary = make_bin_response(request)
		if self._socket is None:
			return
		self._socket.sendall(packed_binary)

	def protocol_handler(self):
		global beam_function_map

		frame = self._socket.recv(1024).strip()

		if not frame:
			return None

		if b'BEAM_CLOSE' in frame:
			self._socket.close()
			return True

		r = parse_bin_request(frame)

		# not enough data or crc mismatch
		if not r:
			logging.debug('Received no valid frames')
			return None

		# run device handler for given device uuid and the cmd
		nts = list(beam_function_map.keys())
		#logging.debug('Bfm keys = {}'.format(nts))
		for t in nts:
			if r.cmd == t.cmd and r.uuid == t.uuid:
				logging.debug('Found a match cmd={}, uuuid={}'.format(r.cmd, r.uuid))
				# should return a BeamResponse namedtuple
				result = beam_function_map[t](r)
				if result:
					packed_binary = make_bin_response(result)
					self._socket.sendall(packed_binary)

	def run(self):

		logging.debug('Starting server thread')
		beam_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		beam_server.bind(('localhost', 50001))
		beam_server.listen(1)

		while True:
			conn, addr = beam_server.accept()
			logging.debug('Incomming connection from {}'.format(addr))
			self._socket = conn
			while True:
				if self._thread_stop:
					break
				if self.protocol_handler():
					self._socket = None
					logging.debug('Connection closed {}'.format(addr))
					break

			if self._thread_stop:
				break

		logging.debug('Stopping server thread')

def signal_handler(signal, frame):
	for d in devices:
		d.stop()
	beam_server.stop()
	logging.info('Stopping beam box simulator')
	sys.exit()

# run simulator with devices initially registered
devices_registered = True

if '--devices=registered' in sys.argv:
	devices_registered = True

# signal handlers
signal.signal( signal.SIGINT, signal_handler )
signal.signal( signal.SIGTERM, signal_handler )

# root logger
logger = logging.getLogger('')
logger.setLevel(logging.DEBUG)

# format for logging
format = logging.Formatter(fmt='%(asctime)s %(levelname)8s [%(module)10s.%(funcName)10s %(lineno)d] %(message)s', datefmt='%b %d %H:%M:%S')

# output to file
stdout = logging.StreamHandler(sys.stdout)
stdout.setFormatter(format)
logger.addHandler(stdout)

logging.info('Starting beam box simulator')

beam_server = BeamServer()

devices.append(Device(beam_server, b'1234567812345678', 'Base', DEVICE_TYPE_BASE, [
	{'id': 2, 'name':'temperature', 		'val': 22.4, 	'mult':10, 'div': 1, 'uom': '°C', 	'min':17.0, 'max':23.0, 'rand':{'diff':30, 'last':0, 'type':'uniform'}},
	{'id': 3, 'name':'fan', 				'val': 3400, 	'mult':1, 'div': 1, 'uom': 'RPM', 	'min':3100, 'max':4300, 'rand':{'diff':10, 'last':0, 'type':'randrange'}},
	{'id': 4, 'name':'cooler_pump', 		'val': 50, 		'mult':1, 'div': 1, 'uom': '%', 	'min':0, 'max':100, 'rand':{'diff':120, 'last':0, 'type':'randrange'}},
	{'id': 5, 'name':'cooler_fan', 			'val': 2300, 	'mult':1, 'div': 1, 'uom': 'RPM', 	'min':1950, 'max':3670, 'rand':{'diff':20, 'last':0, 'type':'randrange'}},
	{'id': 6, 'name':'peltier_duty', 		'val': 35, 		'mult':1, 'div': 1, 'uom': '%', 	'min':0, 'max':100, 'rand':{'diff':45, 'last':0, 'type':'randrange'}},
	{'id': 7, 'name':'peltier_polarity', 	'val': -1, 		'mult':1, 'div': 1, 'uom': '', 		'min': -1, 'max': +1, 'rand':{'diff':300, 'last':0, 'type':'choice'}},
	{'id': 8, 'name':'led_colour', 			'val': 0xc411e4, 'mult':1, 'div': 1, 'uom': '', 	'min':0x000000, 'max':0xffffff, 'rand':{'diff':0, 'last':0, 'type':'none'}},
	{'id': 9, 'name':'led_power', 			'val': 70, 		'mult':1, 'div': 1, 'uom': '%', 	'min':0, 'max':100, 'rand':{'diff':0, 'last':0, 'type':'none'}},
	{'id': 10, 'name':'water_empty', 		'val': 0, 		'mult':1, 'div': 1, 'uom': '', 		'min':0, 'max':1, 'rand':{'diff':400, 'last':0, 'type':'choice'}},
	{'id': 11, 'name':'chamber_overflow', 	'val': 0, 		'mult':1, 'div': 1, 'uom': '', 		'min':0, 'max':1, 'rand':{'diff':60, 'last':0, 'type':'choice'}},
	{'id': 12, 'name':'chamber_pump', 		'val': 1, 		'mult':1, 'div': 1, 'uom': '', 		'min':0, 'max':1, 'rand':{'diff':60, 'last':0, 'type':'choice'}}]))

devices.append(Device(beam_server, b'1111111111111111', 'Chamber 1', DEVICE_TYPE_CHAMBER, [
	{'id': 0, 'name': 'fogger', 'val': 1, 'mult':1, 'div': 1, 'uom': '', 'min':0, 'max':1, 'rand':{'diff':30, 'last':0, 'type':'choice'}},
	{'id': 1, 'name': 'humidity', 'val': 79, 'mult':1, 'div': 1, 'uom': '%', 'min':63, 'max':82, 'rand':{'diff':30, 'last':0, 'type':'randrange'}}]))

devices.append(Device(beam_server, b'2222222222222222', 'Chamber 2', DEVICE_TYPE_CHAMBER, [
	{'id': 0, 'name': 'fogger', 'val': 1, 'mult':1, 'div': 1, 'uom': '', 'min':0, 'max':1, 'rand':{'diff':30, 'last':0, 'type':'choice'}},
	{'id': 1, 'name': 'humidity', 'val': 83, 'mult':1, 'div': 1, 'uom': '%', 'min':77, 'max':90, 'rand':{'diff':30, 'last':0, 'type':'randrange'}}]))

devices.append(Device(beam_server, b'3333333333333333', 'Chamber 3', DEVICE_TYPE_CHAMBER, [
	{'id': 0, 'name': 'fogger', 'val': 1, 'mult':1, 'div': 1, 'uom': '', 'min':0, 'max':1, 'rand':{'diff':30, 'last':0, 'type':'choice'}},
	{'id': 1, 'name': 'humidity', 'val': 67, 'mult':1, 'div': 1, 'uom': '%', 'min':62, 'max':71, 'rand':{'diff':30, 'last':0, 'type':'randrange'}}]))

beam_function_map = {
	BeamFmap(uuid=devices[0].uuid, cmd=REQ_REGISTER_DEVICE): devices[0].register,
	BeamFmap(uuid=devices[0].uuid, cmd=REQ_POLL_DEVICE): devices[0].poll,
	BeamFmap(uuid=devices[0].uuid, cmd=REQ_CONTROL_DEVICE): devices[0].control,

	BeamFmap(uuid=devices[1].uuid, cmd=REQ_REGISTER_DEVICE): devices[1].register,
	BeamFmap(uuid=devices[1].uuid, cmd=REQ_POLL_DEVICE): devices[1].poll,
	BeamFmap(uuid=devices[1].uuid, cmd=REQ_CONTROL_DEVICE): devices[1].control,

	BeamFmap(uuid=devices[2].uuid, cmd=REQ_REGISTER_DEVICE): devices[2].register,
	BeamFmap(uuid=devices[2].uuid, cmd=REQ_POLL_DEVICE): devices[2].poll,
	BeamFmap(uuid=devices[2].uuid, cmd=REQ_CONTROL_DEVICE): devices[2].control,

	BeamFmap(uuid=devices[3].uuid, cmd=REQ_REGISTER_DEVICE): devices[3].register,
	BeamFmap(uuid=devices[3].uuid, cmd=REQ_POLL_DEVICE): devices[3].poll,
	BeamFmap(uuid=devices[3].uuid, cmd=REQ_CONTROL_DEVICE): devices[3].control}

beam_server.start()

for d in devices:
	d.start(devices_registered)

	# simulate devices being added one at a time on the network
	# by waiting until device registers on network before running
	# next device
	if not devices_registered:
		while not d.registered:
			time.sleep(5)

# server and devices run in threads
while True:
	time.sleep(1)
