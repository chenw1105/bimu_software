from setuptools import setup

setup(
    name = "beam",
    version = "0.0.1",
    author = "Vaughn Coetzee",
    author_email = "vaughn.coetzee@centrality.ai",
    description = ("Beam communications and control hub"),
    license = "MIT",
    url = "https://bitbucket.org/centrality",
    packages=['beam'],
    long_description="Communications and control hub for modular grow box containing fogger style root chambers."
)