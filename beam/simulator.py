import socket
import logging
import time
import beam.config as config

HOST_PORT = ('localhost', 50001)

class Transport(object):

	def __init__(self):
		try:
			settings = config.get_config()
			# connect to serial
			self._tcpsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

			if 'sim' in settings and 'host' in settings['sim'] and 'port' in settings['sim']:
				self._tcpsocket.connect((settings['sim']['host'], settings['sim']['port']))
			else:
				self._tcpsocket.connect(HOST_PORT)

			logging.debug("Connected to beam box simulator")

		except socket.timeout as e:
			logging.error(e)
		except Exception as e:
			logging.error(e)
			raise e

	def close(self):
		try:
			self._tcpsocket.sendall(b'BEAM_CLOSE')
			time.sleep(0.3)
			self._tcpsocket.shutdown(socket.SHUT_RDWR)
			self._tcpsocket.close()
		except Exception:
			pass
		logging.debug('Closed socket')

	def receive(self):
		''' Receive data in blocking mode '''
		try:
			recv = self._tcpsocket.recv(1024)
			return recv
		except socket.timeout as e:
			return ''
		except Exception as e:
			logging.error(e)
			return ''

	def send(self, data):
		''' Send data and receive it in blocking mode '''

		try:
			self._tcpsocket.sendall(data)
			time.sleep(0.3)
			recv = self._tcpsocket.recv(1024)
			logging.debug('Received from device {}'.format(recv))
			return recv
		except socket.timeout as e:
			logging.error(e)
		except Exception as e:
			logging.error(e)
