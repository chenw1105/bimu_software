#import ssl
#import certifi
import logging
import sys

from functools import wraps
from pymongo import MongoClient

import beam.config as config

mdb_connection = None
mdb_database = None

def db_valid(func):

	@wraps(func)
	def decorator(*args, **kwargs):

		global mdb_connection
		global mdb_database

		app_config = config.get_config()
		if not mdb_connection:
			logging.debug("Connecting to MongoDB ...")
			if not app_config["mongo"]["ssl"]:
				mdb_connection = MongoClient(app_config["mongo"]["url"])
				logging.debug("Connected to MongoDB")
			'''else:
				self._connection = MongoClient(self._config["database"]["mongo_url"],
					ssl_cert_reqs=ssl.CERT_REQUIRED,
					ssl_ca_certs=certifi.where())'''

			mdb_database = mdb_connection[app_config["mongo"]["database"]]
			if not mdb_database:
				logging.warning("Database not valid")
				return
		return func(*args, **kwargs)
	return decorator

@db_valid
def insert_user(user):
	global mdb_database
	collection = mdb_database["users"]
	cursor = collection.find_one({"id":user["id"]})
	if cursor:
		logging.warning("User already exists")
	else:
		collection.insert_one(user)
		logging.debug("User with id={} created".format(user["id"]))

@db_valid
def delete_user(user_id):
	global mdb_database
	collection = mdb_database["users"]
	collection.delete_one({"id": user_id})

@db_valid
def count_users():
	global mdb_database
	collection = mdb_database["users"]
	cursor = collection.find()
	count = 0
	for i in cursor:
		count += 1
	return count

@db_valid
def get_user(username=None):
	global mdb_database
	collection = mdb_database["users"]
	all_users = []
	if username is None:
		cursor = collection.find({})
		for u in cursor:
			all_users.append(u)
	else:
		cursor = collection.find_one({"email":username})
		if cursor:
			all_users.append(cursor)

	return all_users

@db_valid
def insert_device(device):
	global mdb_database
	collection = mdb_database["devices"]
	collection.insert_one(device)

@db_valid
def get_devices(uuid=None):
	global mdb_database
	collection = mdb_database["devices"]
	all_devices = []
	if uuid is None:
		cursor = collection.find({})
		for d in cursor:
			all_devices.append(d)
	else:
		found = collection.find_one({"uuid": uuid})
		if found:
			all_devices.append(found)

	#logging.debug(all_devices)
	return all_devices

@db_valid
def update_device(uuid, device=None):
	global mdb_database
	collection = mdb_database["devices"]
	collection.update_one(device)

@db_valid
def update_device_meta(uuid, key, value):
	global mdb_database
	collection = mdb_database["devices"]
	collection.update_one({"uuid":uuid}, {"$set":{key:value} })

@db_valid
def update_device_attribute(uuid, attribute, value):
	global mdb_database
	collection = mdb_database["devices"]
	collection.find_and_modify(
		query = {"uuid": uuid, "attributes" : attribute },
		update = { "$set": value }
	)

@db_valid
def clear_devices():
	global mdb_database
	collection = mdb_database["devices"]
	collection.delete_many({})
