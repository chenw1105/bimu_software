import logging
import json
import time
import copy
import os
from functools import wraps

settings = {
	"logging":  {
		"level": 0,
	},
	"uuid": "E777498A-A467-4112-9808-178D38B92B19",
	"tty" : {
		"device": "/dev/ttyS0",
		"baudrate": 57600
	},
	"mongo": {
		"url": "mongodb://127.0.0.1:27017",
		"database": "beam",
		"ssl": False
	},
	"mqtt": {
		"broker": {
			"host": "localhost",
			"port": 8883
		}
	},
	"sim":{
		"enabled": False,
		"host": "localhost",
		"port": 50001
	}
}


def create_base_device(uuid):
	return copy.copy({
		"uuid": uuid,
		"chamber": False,
		"name": "",
		"pos": -1,
		"description":"",
		"attributes": [
				{
					"id": 2,
					"name": "temperature",
					"value": 0.0,
					"mult": 1,
					"div": 10,
					"uom": "°C",
					"time": int(time.time()),
					"bounds": {"min":-45.0, "max":85.0},
					"policy": {
						"change": 0.5,
						"interval": 60
					}
				},
				{
					"id": 3,
					"name": "fan",
					"value": 0,
					"mult": 1,
					"div": 1,
					"uom": "RPM",
					"time": int(time.time()),
					"bounds": {"min":0, "max":5000},
					"policy": {
						"change": 50,
						"interval": 120
					}
				},
				{
					"id": 4,
					"name": "cooler_pump",
					"value": 0,
					"mult": 1,
					"div": 1,
					"uom": "%",
					"time": int(time.time()),
					"bounds": {"min":0, "max":100},
					"policy": {
						"change": 5,
						"interval": 120
					}
				},
				{
					"id": 5,
					"name": "cooler_fan",
					"value": 0,
					"mult": 1,
					"div": 1,
					"uom": "RPM",
					"time": int(time.time()),
					"bounds": {"min":0, "max":5000},
					"policy": {
						"change": 100,
						"interval": 120
					}
				},
				{
					"id": 6,
					"name": "peltier_duty",
					"value": 0,
					"mult": 1,
					"div": 1,
					"uom": "%",
					"time": int(time.time()),
					"bounds": {"min":0, "max":100},
					"policy": {
						"change": 5,
						"interval": 120
					}
				},
				{
					"id": 7,
					"name": "peltier_polarity",
					"value": 0,
					"mult": 1,
					"div": 1,
					"uom": "",
					"time": int(time.time()),
					"bounds": {"min":-1, "max":1},
					"policy": {
						"change": 1,
						"interval": 120
					}
				},
				{
					"id": 8,
					"name": "led_colour",
					"value": 0x000000,
					"mult": 1,
					"div": 1,
					"uom": "RGB",
					"time": int(time.time()),
					"bounds": {"min":0x000000, "max":0xffffff},
					"policy": {
						"change": 0xff,
						"interval": 600
					}
				},
				{
					"id": 9,
					"name": "led_power",
					"value": 0,
					"mult": 1,
					"div": 1,
					"uom": "%",
					"time": int(time.time()),
					"bounds": {"min":0, "max":100},
					"policy": {
						"change": 5,
						"interval": 600
					}
				},
				{
					"id": 10,
					"name": "water_empty",
					"value": 0,
					"mult": 1,
					"div": 1,
					"uom": "",
					"time": int(time.time()),
					"bounds": {"min":0, "max":1},
					"policy": {
						"change": 1,
						"interval": 120
					}
				},
				{
					"id": 11,
					"name": "chamber_overflow",
					"value": 0,
					"mult": 1,
					"div": 1,
					"uom": "",
					"time": int(time.time()),
					"bounds": {"min":0, "max":1},
					"policy": {
						"change": 1,
						"interval": 120
					}
				},
				{
					"id": 11,
					"name": "chamber_pump",
					"value": 0,
					"mult": 1,
					"div": 1,
					"uom": "",
					"time": int(time.time()),
					"bounds": {"min":0, "max":1},
					"policy": {
						"change": 1,
						"interval": 120
					}
				}
			]
		}
	)

def create_chamber_device(uuid):
	return copy.copy({
		"uuid": uuid,
		"chamber": True,
		"name": "",
		"pos" : 0,
		"description":"",
		"attributes": [
				{
					"id": 0,
					"name": "fogger",
					"value": 0,
					"mult": 1,
					"div": 1,
					"uom": "",
					"time": int(time.time()),
					"bounds": {"min":0, "max":1},
					"policy": {
						"change": 1,
						"interval": 120
					}
				},
				{
					"id": 1,
					"name": "humidity",
					"value": 0,
					"mult": 1,
					"div": 1,
					"uom": "%RH",
					"time": int(time.time()),
					"bounds": {"min":0, "max":100},
					"policy": {
						"change": 2,
						"interval": 60
					}
				}
			]
		})

def load_config():
	global settings

	if os.path.exists("/beam/settings.json"):

		try:
			with open("/beam/settings.json", 'r') as f:
				settings = json.load(f)
		except Exception as e:
			logging.warning(e)

	else:
		logging.warning("No settings.json file")

def get_config():
	global settings
	return settings
