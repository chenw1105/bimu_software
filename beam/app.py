import json
import logging
from logging.handlers import TimedRotatingFileHandler
import signal
import sys
import time

from beam import messenger, control, users, discovery, config



class BeamApp():

	def __init__(self, log_level=None, log_dest=None, log_dest_path=None):

		# setup signal handler
		signal.signal( signal.SIGINT, self.stop )
		signal.signal( signal.SIGTERM, self.stop )

		app_settings = config.get_config()

		# setup logging
		logger = logging.getLogger('')
		if log_level is None or log_level < logging.DEBUG or log_level > logging.CRITICAL:
			logger.setLevel(app_settings["logging"]["level"])
		else:
			logger.setLevel(log_level)

		format = logging.Formatter(fmt='%(asctime)s %(levelname)8s [%(module)10s.%(funcName)10s %(lineno)d] %(message)s', datefmt='%b %d %H:%M:%S')

		if log_dest == 'file':
			if log_dest_path:
				handler = TimedRotatingFileHandler(log_dest_path, when="midnight", interval=1, backupCount=14, encoding=None, delay=True, utc=False)
			else:
				handler = TimedRotatingFileHandler("/var/log/beam/beam.log", when="midnight", interval=1, backupCount=14, encoding=None, delay=True, utc=False)

			handler.setFormatter(format)
			logger.addHandler(handler)
		else:
			handler = logging.StreamHandler(sys.stdout)
			handler.setFormatter(format)
			logger.addHandler(stdout_handler)

		# setup app services

		self._controller = control.Controller(simulated=app_settings['sim']['enabled'])
		self._messenger = messenger.Messenger()
		self._messenger.register_endpoint(in_topic="discovery/SEND", out_topic="discovery/REPLY", endpoint=discovery.discover_ep)
		self._messenger.register_endpoint(in_topic="{}/user/register/SEND".format(app_settings["uuid"]),	out_topic="{}/user/register/REPLY".format(app_settings["uuid"]), endpoint=users.register)
		self._messenger.register_endpoint(in_topic="{}/user/login/SEND".format(app_settings["uuid"]), out_topic="{}/user/login/REPLY".format(app_settings["uuid"]), endpoint=users.login)
		self._messenger.register_endpoint(in_topic="{}/user/add/SEND".format(app_settings["uuid"]), out_topic="{}/user/add/REPLY".format(app_settings["uuid"]), endpoint=users.add)
		self._messenger.register_endpoint(in_topic="{}/user/edit/SEND".format(app_settings["uuid"]), out_topic="{}/user/edit/REPLY".format(app_settings["uuid"]), endpoint=users.edit)
		self._messenger.register_endpoint(in_topic="{}/user/delete/SEND".format(app_settings["uuid"]), out_topic="{}/user/delete/REPLY".format(app_settings["uuid"]), endpoint=users.delete)
		self._messenger.register_endpoint(in_topic="{}/user/get/SEND".format(app_settings["uuid"]), out_topic="{}/user/get/REPLY".format(app_settings["uuid"]), endpoint=users.get)
		self._messenger.register_endpoint(in_topic="{}/devices/configure/POST".format(app_settings["uuid"]), out_topic="{}/devices/configure/REPLY".format(app_settings["uuid"]), endpoint=self.controller.configure_chamber)

		'''
		self._messenger.register_endpoint(in_topic="{}/devices/control/POST".format(app_settings["uuid"]), out_topic="{}/devices/control/REPLY".format(app_settings["uuid"]), endpoint=self.controller.set_device)
		self._messenger.register_endpoint(in_topic="{}/devices/discover/GET".format(app_settings["uuid"]), out_topic="{}/devices/discover/REPLY".format(app_settings["uuid"]), endpoint=self.controller.get_device)
		'''


	def stop(signal, frame):
		logging.info("Caught signal {}, exiting beam hub server".format(signal))
		self._controller.stop()
		self._messenger.stop()
		sys.exit()

	def run():
		logging.info("Starting beam hub server")
		self._messenger.start()
		self._controller.start()
