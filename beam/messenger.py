import json
import logging
import time
import threading
from functools import wraps

import paho.mqtt.client as mqtt

import beam.config as config

messenger = None

class Messenger(object):

	def __init__(self):
		app_config = config.get_config()
		self._beam_uuid = app_config["uuid"]
		self._endpoints = {}
		self._mqtt = mqtt.Client(client_id='BEAM-{}'.format(self._beam_uuid))
		self._mqtt.on_connect = self.connected
		self._mqtt.on_publish = self.message_sent
		self._mqtt.on_message = self.message_received
		self._mqtt.connect(app_config["mqtt"]["broker"]["host"], app_config["mqtt"]["broker"]["port"])

	def start(self):
		self._mqtt.loop_start()
		pass

	def stop(self):
		self._mqtt.loop_stop()
		pass

	def register_endpoint(self, in_topic, out_topic, endpoint):
		if in_topic not in self._endpoints:
			self._endpoints[in_topic] = (out_topic, endpoint)

	def publish_message(self, topic, message):
		''' Function callback so board can publish data '''

		logging.debug("Publish msg topic={} payload={}".format(topic, message))
		self._mqtt.publish(topic=topic, payload=json.dumps(message))

	def connected(self, client, userdata, flags, rc):
		''' Mqtt client connected to broker '''

		logging.debug("Connected to MQTT broker")

		for in_topic in self._endpoints:
			logging.debug("Subscribing to topic {} [Tuple = {}]".format(in_topic, self._endpoints[in_topic]))
			self._mqtt.subscribe(in_topic)

	def message_received(self, client, userdata, msg):
		''' Mqtt client received message from broker '''

		logging.debug("Received MQTT msg: topic={}, payload={}, qos={}, retain={}".format(msg.topic, msg.payload, msg.qos, msg.retain))

		if msg.topic in self._endpoints:
			data = json.loads(msg.payload.decode('utf-8'))
			#logging.debug("Calling endpoint for topic {} with payload {}".format(msg.topic, data))
			reply = self._endpoints[msg.topic][1](data)

			logging.debug("Reply from endpoint {}".format(reply))

			if not reply:
				logging.debug("No endpoint reply")
				return

			# check outgoing topic is valid, then publish using the reply topic
			if self._endpoints[msg.topic][0]:
				self.publish_message(self._endpoints[msg.topic][0], reply)
			else:
				logging.debug("No outgoing topic for incomming topic {}".format(msg.topic))
		else:
			logging.warning("Topic {} not endpoints".format(msg.topic))

	def message_sent(self, client, userdata, mid):
		''' Mqtt client published message to broker
			mid is the message id '''

		logging.debug("Published MQTT msg: Mid = {}".format(mid))
