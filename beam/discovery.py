import logging
import beam.config as config


def discover_ep(payload):

	logging.debug("Discover beam endpoint")
	reply = {
		"success": True,
		"data": {
			"gatewayId": config.get_config()["uuid"]
		},
		"error": ""
	}
	return reply
