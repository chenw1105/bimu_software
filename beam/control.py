import sys
import threading
import queue
import signal
import time
import logging
import json
import collections
import struct
import binascii
from functools import wraps

import beam.users as users
import beam.messenger as messenger
import beam.config as config
import beam.database as database



DEVICE_TYPE_BASE = 0
DEVICE_TYPE_CHAMBER = 1

REQ_ADVERTISE_TO_MASTER = 0
REQ_REGISTER_DEVICE = 1
REQ_POLL_DEVICE = 2
REQ_CONTROL_DEVICE = 3

REQ_MIN = REQ_ADVERTISE_TO_MASTER
REQ_MAX = REQ_CONTROL_DEVICE

REQ_HEADER_LENGTH = 21
RSP_HEADER_LENGTH = 22

BEAM_ATTRIBUTES = [
	'fogger',
	'humidity',
	'temperature',
	'fan',
	'cooler_pump',
	'cooler_fan',
	'peltier_duty',
	'peltier_polarity',
	'led_colour',
	'led_power',
	'water_empty',
	'chamber_overflow'
	'chamber_pump']

NULL_ADDR = b'00000000000000000000'

BeamRequest = collections.namedtuple('BeamRequest', 'cmd, uuid, plen, payload')
BeamResponse = collections.namedtuple('BeamResponse', 'cmd, uuid, result, plen, payload')
BeamFmap = collections.namedtuple('BeamFmap', 'uuid, cmd')

def action(func):
	@wraps(func)
	def wrapper(*args, **kwargs):
		data = func(*args, **kwargs)

		if data:
			result = {
				'success': True,
				'data': {'devices':[]},
				'error': ''
			}
			result['data']['devices'].append(data)

			# publish to message broker
			messenger.send_message(topic="{}\devices\poll\REPLY".format(config.get_config()['uuid']), payload=result)

			# dump to database
			device_uuid = args[1]['uuid']
			logging.debug('Update device {} attribute {} in database'.format(device_uuid, args[2]))
			database.update_device_attribute(device_uuid, args[2], args[3])
	return wrapper

def validate_payload(has_data=False):
	def decorator(func):
		@wraps(func)
		def wrapper(*args, **kwargs):
			payload = args[0]

			if has_data:
				if 'data' not in payload:
					error_str = 'Payload missing data key'
					logging.warning(error_str)
					return {'success': False, data:{}, 'error':error_str}

				if len(payload['data']) == 0:
					error_str = 'Payload data value empty'
					logging.warning(error_str)
					return {'success': False, data:{}, 'error':error_str}

			return func(*args, **kwargs)
		return wrapper
	return decorator

def pack_request(beam_request):
	''' Pack a binary frame from a BeamRequest namedtuple to send to a device '''

	if beam_request.plen:
		message = struct.pack('<B16sH{}B'.format(beam_request.plen), beam_request.cmd, beam_request.uuid, beam_request.plen, beam_request.payload)
		crc16 = binascii.crc_hqx(message, 0)
		return struct.pack('<B16sH{}BH'.format(beam_request.plen), beam_request.cmd, beam_request.uuid, beam_request.plen, beam_request.payload, crc16)
	else:
		message = struct.pack('<B16sH',beam_request.cmd, beam_request.uuid, 0x0000)
		crc16 = binascii.crc_hqx(message, 0)
		return struct.pack('<B16sHH', beam_request.cmd, beam_request.uuid, 0x0000, crc16)

def unpack_response(frame):
	''' Unpack a binary frame from a device and returns a BeamResponse namedtuple '''

	data_length = len(frame)

	# Is there enough to parse header
	if data_length < RSP_HEADER_LENGTH:
		logging.info('Response header length invalid, got {}'.format(data_length))
		return None

	# checksum calculation and comparison
	plen = data_length - RSP_HEADER_LENGTH

	if plen != 0:
		data = struct.unpack('<B16sBH{}sH'.format(plen), frame)
		claim = struct.pack('<B16sBH{}s'.format(plen), data[0], data[1], data[2], data[3], data[4])
		crc16 = binascii.crc_hqx(claim, 0)
		if crc16 != data[5]:
			logging.info("Crc mismatch, got {} but calculated {}".format(data[5], crc16))
			frame = ''
			return None

		logging.debug('Unpack response: Cmd={}, uuid={}, result={}, plen={}, payload={}'.format(data[0], data[1], data[2], data[3], data[4]))
		return BeamResponse(cmd=data[0], uuid=data[1], result=data[2], plen=data[3], payload=data[4])
	else:
		data = struct.unpack('<B16sBHH', frame)
		claim = struct.pack('<B16sBH'.format(plen), data[0], data[1], data[2], data[3])
		crc16 = binascii.crc_hqx(claim, 0)
		if crc16 != data[4]:
			logging.info("Crc mismatch, got {} but calculated {}".format(data[4], crc16))
			frame = ''
			return None

		logging.debug('Unpack response: Cmd={}, uuid={}, result={}, plen=0, payload=None'.format(data[0], data[1], data[2]))
		return BeamResponse(cmd=data[0], uuid=data[1], result=data[2], plen=0, payload=None)

class Controller(object):
	''' Controls the network of devices in the grow box '''

	def __init__(self, simulated=False):

		if simulated:
			from beam.simulator import Transport
			self._transport = Transport()
		else:
			from beam.link import Transport
			self._transport = Transport()

		# response handlers from slave devices
		self._endpoints = {
			REQ_ADVERTISE_TO_MASTER: self.advertise_ep,
			REQ_REGISTER_DEVICE: self.register_ep,
			REQ_POLL_DEVICE: self.poll_device_ep,
			REQ_CONTROL_DEVICE: self.control_device_ep}

		# network discovery mode
		self._discovery_mode = False

		# devices dictionary containing uuid:config ,apping
		self._devices = {}

		# for polling individual devices by address
		self._addrq = queue.Queue(maxsize=13)

		# request queue for data coming from messenger
		self._queue = queue.Queue(maxsize=30)

	def ping(self):
		return 'Pong'

	def discover_devices(self):
		recv = self._transport.receive()
		logging.debug('Received {}'.format(recv))
		self.handle_response(recv)

	def start(self):

		idle = 1
		logging.debug('Controller start')

		# if no devices in factory, go into discovery mode
		self.factory()

		while True:

			time.sleep(0.5)

			# check jobs in queue
			data = None
			try:
				if not self._queue.empty():
					data = self._queue.get()
					self._queue.task_done()
			except Exception as e:
				logging.warning(e)
				continue

			# send data on wire
			if data:
				logging.debug('Sending data {} to device'.format(data))
				recv = self._transport.send(data)
				self.handle_response(recv)

			# discover devices
			if self._discovery_mode:
				self.discover_devices()

			# poll devices
			else:
				self.poll_devices()

	def stop(self):
		self._transport.close()

	def new_request(self, data):
		''' Put new data request on queue '''

		try:
			self._queue.put(data)
		except queue.Full as e:
			logging.error('Queue full')
			return

	def factory(self):

		# check if any devices registered in database yet
		devices = database.get_devices()
		logging.debug(devices)
		if not devices:
			self._discovery_mode = True
			logging.info('No devices registered in database yet')

		# create device dictionaries
		for device in devices:
			self._devices[device['uuid']] = device

	def reset_addrq(self):

		# keys in the dictionary are the device uuids
		uuidlist = list(self._devices.keys())

		for device_uuid in uuidlist:
			self._addrq.put(device_uuid)

	def handle_response(self,recv):

		if not recv:
			logging.debug('No data received')
			return

		# BeamResponse namedtuple returned from parse_bin_response
		device_response = unpack_response(recv)

		if not device_response:
			return

		if device_response.result:
			logging.info('Device request failed with error = {}'.format(device_response.result))
			return

		# check cmd is a valid one
		if device_response.cmd < REQ_MIN or device_response.cmd > REQ_MAX:
			logging.info('Unknown command {}'.format(device_response.cmd))
			return

		# call response handler
		self._endpoints[device_response.cmd](device_response.uuid, device_response.payload, device_response.plen)

	def register_device(self, uuid):
		message = pack_request(BeamRequest(cmd=REQ_REGISTER_DEVICE, uuid=uuid, payload=None, plen=0))
		self.new_request(message)

	def poll_devices(self):

		# pop next addr to poll
		if self._addrq.empty():
			self.reset_addrq()

			# still empty after reset so probably no devices registered yet
			if self._addrq.empty():
				return

		next_addr = self._addrq.get()
		message = pack_request(BeamRequest(cmd=REQ_POLL_DEVICE, uuid=next_addr, payload=None, plen=0))
		self.new_request(message)

	@action
	def update_device_attribute(self, device, attr_id, attr_value):
		''' @action for publishing attribute changes according to its policy '''

		for attr in device['attributes']:
			if attr['id'] == attr_id:

				value = attr_value * attr['mult'] / attr['div']

				# check bounds
				if value < attr['bounds']['min'] or value > attr['bounds']['max']:
					logging.warning('Attribute value out of bounds')
					return None

				# check policy to publish or not
				publish = False
				if abs(value - attr['value']) > attr['policy']['change']:
					logging.debug('Attribute {} changed from {}{} to {}{}'.format(attr['name'], attr['value'], attr['uom'], value, attr['uom']))
					publish = True
				elif attr['policy']['interval'] > 0:
					if time.time() - attr['time'] > attr['policy']['interval']:
						logging.debug('Attribute {} last interval {}'.format(attr['name'], time.time() - attr['time']))
						attr['time'] = int(time.time())
						publish = True

				# update attribute with new value
				attr['value'] = value
				#logging.debug('Device uuid={} attribute={} update to {} {}'.format(device['uuid'], attr['name'], value, attr['uom']))

				if publish:
					return { 'uuid': device['uuid'].decode('utf-8'), 'attribute': attr['name'], 'value': attr['value']}
				else:
					return None


	''' ---------------------------------------- '''
	''' Slave device response handlers '''
	''' ---------------------------------------- '''

	def advertise_ep(self, uuid, payload, plen):

		logging.info('Device {} advertising itself'.format(uuid))
		self.register_device(uuid)


	def register_ep(self, uuid, payload, plen):
		''' payload contains a byte field which indicates whether the device is base or chamber type '''

		# return tuple
		device_type = struct.unpack('<B', payload)[0]

		# keys in the dictionary are the device uuids
		uuidlist = list(self._devices.keys())

		# total devices registered
		total = len(uuidlist)

		logging.debug('Register ep uuid={}, device_type={}, total devices so far {}'.format(uuid, device_type, total))

		# create dictionary object of the device
		if uuid not in uuidlist:
			# chamber device
			if device_type == DEVICE_TYPE_CHAMBER:
				self._devices[uuid] = config.create_chamber_device(uuid)
				self._devices[uuid]['pos'] = total
				logging.debug('Created new chamber device {} at position {}'.format(uuid, total))

			# base device
			elif device_type == DEVICE_TYPE_BASE:
				self._devices[uuid] = config.create_base_device(uuid)
				self._devices[uuid]['pos'] = 0
				logging.debug('Created new base device {}'.format(uuid))

		# check if registered in database
		devices = database.get_devices(uuid)
		for device in devices:
			if uuid == device['uuid']:
				logging.debug('Device already registered')
				return

		logging.debug('Create and registered device {}'.format(self._devices[uuid]))
		database.insert_device(self._devices[uuid])

	def poll_device_ep(self, uuid, payload, plen):
		''' Payload is a sequence of Attribute_Id(1 byte),Attribute_Value(32bit integer)'''

		logging.debug('Poll device ep uuid={}, payload={}, plen={}'.format(uuid, payload, plen))

		if plen % 5 != 0:
			logging.warning('Expecting 5 bytes per attribute but plen = {}'.format(plen))
			return

		# iteratively unpack
		attributes = struct.iter_unpack('<Bi', payload)

		# keys in the dictionary are the device uuids
		uuidlist = list(self._devices.keys())

		for device_uuid in uuidlist:
			if uuid == device_uuid:
				# iteratively update each device attribute
				while True:
					try:
						att_tuple = next(attributes)
						self.update_device_attribute(self._devices[uuid], att_tuple[0], att_tuple[1])
					except StopIteration:
						break

	def control_device_ep(self, uuid, payload, plen):
		logging.debug('Control message response for device {} received'.format(uuid))

	''' ------------------------------------- '''
	''' Messenger facing topic endpoints '''
	''' ------------------------------------- '''

	@users.verify_token
	def discovery_off(self, payload):
		''' Turn off device discovery now '''

		logging.info('Turning off discovery mode')
		self._discovery_mode = False
		return {'success':True, data:{}, 'error':''}

	@users.verify_token
	@validate_payload(has_data=True)
	def configure_chamber(self, payload):
		''' Provision new device if not already registered in database or
			modify device properties if already registered '''

		for device in payload['data']:
			if device['uuid'] not in self._devices:

				# provision new device (can only provision chamber devices)
				self._devices[device['uuid']] = config.create_chamber_device(device['uuid'])

				# register in database
				database.insert_device(self._devices[device['uuid']])

			else:
				# TODO validate data here
				self._devices[device['uuid']] = device

				database.update_device(self._devices[devices['uuid']])

		return {'success':True, data:{}, 'error':''}

	@users.verify_token
	@validate_payload(has_data=True)
	def set_device(self, payload):

		for device in payload['data']:
			if 'uuid' not in device:
				error_str = 'Payload data device missing uuid key'
				logging.warning(error_str)
				return {'success': False, data:{}, 'error':error_str}

			if 'attributes' not in device:
				error_str = 'Payload data device missing attributes key'
				logging.warning(error_str)
				return {'success': False, data:{}, 'error':error_str}

			for d in self._devices:
				if device['uuid'] == d['uuid']:
					message = pack_request(BeamRequest(cmd=REQ_CONTROL_DEVICE, uuid=device['uuid'], payload=[payload['attribute'], payload['value']], payload_length=5))
					self.new_request((REQ_SET_DEVICE, address, message))

	@users.verify_token
	def get_device(self, payload):
		return {'success': True, 'data':{'devices':self._devices}, 'error':''}
