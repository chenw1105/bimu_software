import struct
import binascii
import logging
import os
import serial
import time
import beam.config as config

RTS = 21

def gpioExport(gpio):
	try:
		gpiopath = '/sys/class/gpio/gpio{}'.format(gpio)
		if not os.path.isfile(gpiopath):
			with open('/sys/class/gpio/export', 'w') as f:
				f.write('{}'.format(gpio))
	except Exception as e:
		logging.error(e)

def gpioDirection(gpio, direction):
	try:
		gpiopath = '/sys/class/gpio/gpio{}/direction'.format(gpio)

		# export if it does not exist
		if not os.path.isfile(gpiopath):
			gpioExport(gpio)

		# set direction
		with open(gpiopath, 'w') as f:
			f.write(direction)

	except Exception as e:
		logging.error(e)

def gpioValue(gpio, value):
	try:
		gpiopath = '/sys/class/gpio/gpio{}/value'.format(gpio)

		# set value
		with open(gpiopath, 'w') as f:
			f.write('{}'.format(value))

	except Exception as e:
		logging.error(e)

def gpioState(gpio):
	state = ''
	try:
		gpiopath = '/sys/class/gpio/gpio{}/value'.format(gpio)

		# set value
		with open(gpiopath, 'r') as f:
			state = f.read()

		state = state.strip()

		if state == '0':
			return 0
		elif state == '1':
			return 1
		else:
			return -1

	except Exception as e:
		logging.error(e)
		return -1

def set_rts(enabled=False):
	if enabled:
		gpioValue(RTS, 1)
	else:
		gpioValue(RTS,0)




class Transport(object):

	def __init__(self):
		try:
			settings = config.get_config()
			# connect to serial
			self._serial = serial.Serial(port=settings["tty"]["device"],
											baudrate=settings["tty"]["baudrate"],
											timeout=1.0,
											write_timeout=1.0)
			logging.debug("Serial open")
			# set power pin as output
			gpioDirection(RTS, "out")
				# set low, should be low from hardware strapping and kernel pin mux setup anyway
			gpioValue(RTS,0)
		except serial.SerialException as e:
			logging.error(e)
			raise
		except Exception as e:
			logging.error(e)
			raise e

	def close(self):
		self._serial.close()

	def receive(self):
		''' Receive data in blocking mode '''
		try:
			set_rts(enabled=False)

			time.sleep(0.1)

			# count data waiting in line buffer
			count = self._serial.in_waiting
			if count:
				# read blocking
				return self._serial.read(count)
			else:
				return ''
		except socket.timeout as e:
			return ''
		except Exception as e:
			logging.error(e)
			return ''

	def send(self, data):
		''' Send data and receive it in blocking mode '''

		try:
			# driver enable, receive disable
			set_rts(enabled=True)
			# write blocking
			self._serial.write(data)
			time.sleep(0.01)
			# receive enable, driver disable
			set_rts(enabled=False)

			mark = time.time()
			data = b''
			while True:

				if time.time() - mark > 2:
					break

				# count data waiting in line buffer
				count = self._serial.in_waiting
				if count:
					# read blocking
					data += self._serial.read(count)

			return data

		except serial.SerialTimeoutException as e:
			return ''
		except serial.SerialException as e:
			logging.error(e)
			return ''
		except Exception as e:
			logging.error(e)
			return ''
