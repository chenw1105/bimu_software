import cmd
import sys
import os
import json
import time
import random
import collections
from functools import wraps
import paho.mqtt.client as mqtt

Tapi = collections.namedtuple('Tapi', 'topic, handler')

settings = None
apis = {}
beam_uuid = None
user_id = None
user_token = None

def load_settings():
	global settings

	if os.path.exists('/beam/settings.json'):

		try:
			with open('/beam/settings.json', 'r') as f:
				settings = json.load(f)
		except Exception as e:
			raise e

	else:
		raise Exception('No settings')

def validate_payload(keys=[],list_of_items=False):
	def decorator(func):
		@wraps(func)
		def wrapper(*args, **kwargs):

			topic = args[1]
			payload = args[2]

			if not all(key in payload for key in ['success', 'data', 'error']):
				print('Payload invalid for {}'.format(topic))
				return

			if not payload['success']:
				print('Api {} response failed with {}'.format(topic, payload['error']))
				return

			if list_of_items:
				for item in payload['data']:
					if not all(x in item for x in keys):
						print('Payload missing fields in data')
						return
			else:
				if not all(x in payload['data'] for x in keys):
					print('Payload missing fields in data')
					return

			func(*args, **kwargs)

		return wrapper
	return decorator

def validate_args(options=[]):
	def decorator(func):
		@wraps(func)
		def wrapper(*args, **kwargs):

			cli_args = args[1]
			argmap = parse(cli_args)

			if len(argmap) == 0 and len(options) != 0:
				print('No options given, expecting {}'.format(options))
				return

			for opt in options:
				missing = True
				for a in argmap:
					if opt == a[0]:
						missing = False

				if missing:
					print('Missing option {}'.format(opt))
					return

			func(*args, **kwargs)

		return wrapper
	return decorator

def validate_beam(func):
	@wraps(func)
	def wrapper(*args, **kwargs):
		global beam_uuid
		if beam_uuid is None:
			print('Beam uuid not set, use set_beam <uuid> command')
			return
		func(*args, **kwargs)
	return wrapper

def validate_user(func):
	@wraps(func)
	def wrapper(*args, **kwargs):
		global user_id, user_token
		if user_id is None:
			print('Beam user id not set, use set_user <id> <token> command')
			return
		if user_token is None:
			print('Beam user token not set, use set_user <id> <token> command')
			return

		func(*args, **kwargs)
	return wrapper

class MqttClient():

	def __init__(self):
		self._mqtt = mqtt.Client(client_id='Beam-cli')
		self._mqtt.on_connect = self.on_connect
		self._mqtt.on_publish = self.message_sent
		self._mqtt.on_message = self.message_received
		self._mqtt.connect("localhost", 8883)

	def on_connect(self, client, userdata, flags, rc):
		print('Connected to beam')

	def start(self):
		self._mqtt.loop_start()

	def stop(self):
		self._mqtt.disconnect()
		self._mqtt.loop_stop()

	def subscribe(self, topic):
		self._mqtt.subscribe(topic)

	def publish_message(self, topic, message):
		#logging.debug('Publish msg topic={} payload={}'.format(topic, message))
		self._mqtt.publish(topic=topic, payload=json.dumps(message))

	def message_received(self, client, userdata, msg):
		global apis
		#print('Received MQTT msg: topic={}, payload={}, qos={}, retain={}'.format(msg.topic, json.loads(msg.payload.decode('utf-8')), msg.qos, msg.retain))
		tuplist = list(apis.values())
		for tup in tuplist:
			if msg.topic in tup.topic:
				tup.handler.response(msg.topic, json.loads(msg.payload.decode('utf-8')))
			#else:
			#	print('No topic handler {} <> {}'.format(msg.topic, tup.topic))

	def message_sent(self, client, userdata, mid):
		pass
		#logging.debug('Published MQTT msg: Mid = {}'.format(mid))

class DiscoverApi():

	def send(self):
		global mqtt_client
		mqtt_client.subscribe('discovery/REPLY')
		mqtt_client.publish_message(topic='discovery/SEND', message={})

	@validate_payload(keys=['gatewayId'])
	def response(self, topic, payload):

		if len(payload['data']) == 0:
			print('No beam boxes discovered')
			return

		print('Beam discovered:')
		print('	UUID={}'.format(payload['data']['gatewayId']))

class UserRegisterApi():

	def send(self, uuid, firstname, lastname, email, password, mobileid):
		global mqtt_client
		mqtt_client.subscribe('{}/user/register/REPLY'.format(uuid))
		mqtt_client.publish_message(topic='{}/user/register/SEND'.format(uuid),
			message={'firstName':firstname,
				'lastName': lastname,
				'email':email,
				'password': password,
				'deviceId':mobileid})

	@validate_payload(keys=['id', 'firstName', 'lastName', 'isAdmin', 'token', 'settings', 'deviceId'])
	def response(self, topic, payload):
		print('User registered:')
		print('	Id={}, firstname={}, lastname={}, mobileId={}, token={}, admin={}'.format(
			payload['data']['id'],
			payload['data']['firstName'],
			payload['data']['lastName'],
			payload['data']['deviceId'],
			payload['data']['token'],
			payload['data']['isAdmin']))

class UserLoginApi():

	def send(self, uuid, username, password, mobileid):
		global mqtt_client
		mqtt_client.subscribe('{}/user/login/REPLY'.format(uuid))
		mqtt_client.publish_message(topic='{}/user/login/SEND'.format(uuid),
			message={'username':username,
				'password': password,
				'deviceId':mobileid})

	@validate_payload(keys=['id', 'firstName', 'lastName', 'isAdmin', 'token', 'settings', 'deviceId'])
	def response(self, topic, payload):
		print('User logged in:')
		print('	Id={}, firstname={}, lastname={}, mobileId={}, token={}, admin={}'.format(
			payload['data']['id'],
			payload['data']['firstName'],
			payload['data']['lastName'],
			payload['data']['deviceId'],
			payload['data']['token'],
			payload['data']['isAdmin']))

class UserDeleteApi():

	def send(self, uuid, id, token):
		global mqtt_client
		mqtt_client.subscribe('{}/user/delete/REPLY'.format(uuid))
		mqtt_client.publish_message(topic='{}/user/delete/SEND'.format(uuid),
			message={'userId':id,
				'token': token})

	@validate_payload(keys=['id', 'firstName', 'lastName', 'isAdmin'], list_of_items=True)
	def response(self, topic, payload):
		print('User deleted:')
		for u in payload['data']:
			print('	Id={}, firstname={}, lastname={}, admin={}'.format(
				u['id'],
				u['firstName'],
				u['lastName'],
				u['isAdmin']))

class UserGetApi():

	def send(self, uuid, id, token):
		global mqtt_client
		mqtt_client.subscribe('{}/user/get/REPLY'.format(uuid))
		mqtt_client.publish_message(topic='{}/user/get/SEND'.format(uuid),
			message={'userId':id,
				'token': token})

	@validate_payload(keys=['id', 'firstName', 'lastName', 'isAdmin'], list_of_items=True)
	def response(self, topic, payload):
		print('Users registered:')
		for user in payload['data']:
			print('	id={}, firstname={}, lastname={}, admin={}'.format(
				user['id'],
				user['firstName'],
				user['lastName'],
				user['isAdmin']))

class BeamShell(cmd.Cmd):

	intro = 'Welcome to the Beam cli. Type help or ? to list commands.\n'
	prompt = '(beam) '
	file = None

	def do_get_beam_id(self, arg):
		''' Get beam uuid '''
		global apis
		if not 'discover' in apis:
			apis['discover'] = Tapi('discovery/REPLY', DiscoverApi())
		apis['discover'].handler.send()

	@validate_beam
	@validate_args(options=['first', 'last', 'email', 'pass', 'mobid'])
	def do_register_user(self, arg):
		''' Register a user to beam box [args: \
			user firstname <first>, \
			user lastname <last>, \
			user email address <email>, \
			user password <pass>,
			user mobile device id <mobid> ]'''
		global beam_uuid
		argmap = parse(arg)
		first = ''
		last = ''
		email = ''
		passw = ''
		mobid = ''

		for a in argmap:
			if 'first'in a[0]:
				first = a[1]
			if 'last' in a[0]:
				last = a[1]
			if 'email' in a[0]:
				email = a[1]
			if 'pass' in a[0]:
				passw = a[1]
			if 'mobid' in a[0]:
				mobid = a[1]

		print('Register user to beam={}: first={}, last={}, email={}, password={}, mobileId={}'.format(
			beam_uuid,
			first,
			last,
			email,
			passw,
			mobid))

		global apis
		if not 'register' in apis:
			apis['register'] = Tapi('{}/user/register/REPLY'.format(beam_uuid), UserRegisterApi())
		apis['register'].handler.send(beam_uuid, first, last, email, passw, mobid)

	@validate_beam
	@validate_args(options=['user', 'pass', 'mobid'])
	def do_login_user(self, arg):
		''' User login [args: <user>, <pass>, <mobid>] '''

		global beam_uuid
		argmap = parse(arg)
		username = ''
		passw = ''
		mobid = ''

		for a in argmap:
			if 'user'in a[0]:
				username = a[1]
			if 'pass' in a[0]:
				passw = a[1]
			if 'mobid' in a[0]:
				mobid = a[1]

		print('User login, beam_uuid={}, username={}, password={}, mobileId={}'.format(beam_uuid, username, passw, mobid))

		global apis
		if 'login' not in apis:
			apis['login'] = Tapi('{}/user/login/REPLY'.format(beam_uuid), UserLoginApi())
		apis['login'].handler.send(beam_uuid, username, passw, mobid)

	@validate_beam
	@validate_user
	def do_get_users(self, arg):
		''' Get all users registered to beam box '''

		global beam_uuid, user_id, user_token

		print('Get all users, beam_uuid={}, user_id={}, user_token={}'.format(beam_uuid, user_id, user_token))

		global apis
		if 'get_users' not in apis:
			apis['get_users'] = Tapi('{}/user/get/REPLY'.format(beam_uuid), UserGetApi())
		apis['get_users'].handler.send(beam_uuid, user_id, user_token)

	@validate_beam
	@validate_user
	def do_delete_user(self, arg):
		''' Delete a user from beam box '''

		global beam_uuid, user_id, user_token

		print('Delete user, beam_uuid={}, user_id={}, user_token={}'.format(beam_uuid, user_id, user_token))

		global apis
		if 'delete' not in apis:
			apis['delete'] = Tapi('{}/user/delete/REPLY'.format(beam_uuid), UserDeleteApi())
		apis['delete'].handler.send(beam_uuid, user_id, user_token)

	@validate_beam
	@validate_user
	@validate_args(options=['temp'])
	def do_set_temperature(self, arg):
		''' Set temperature of beam box [args: <temp> ] '''

		global beam_uuid, user_id, user_token
		argmap = parse(arg)
		temp = None
		for a in argmap:
			if 'temp' in a[0]:
				temp = float(a[1])

		print('Set temperature, beam_uuid={}, user_id={}, user_token={}, temp={:2.1f}'.format(beam_uuid, user_id, user_token, temp))

	@validate_beam
	@validate_user
	@validate_args(options=['chid', 'humidity'])
	def do_set_humidity(self, arg):
		''' Set humidity level in root chamber [args: <chid>, <humidity> ] '''

		global beam_uuid, user_id, user_token
		argmap = parse(arg)
		chid = None
		humidity = None
		for a in argmap:
			if 'chid' in a[0]:
				chid = a[1]
			if 'humidity' in a[0]:
				humidity = int(a[1])

		print('Set humidity, beam_uuid={}, user_id={}, user_token={}, chamber_id={}, humidity={}'.format(beam_uuid, user_id, user_token, chid, humidity))

	@validate_args(options=['uuid'])
	def do_set_beam(self, arg):
		''' Set the beam for which all subsequent commands reference [args: <uuid>] '''
		global beam_uuid
		argmap = parse(arg)
		for a in argmap:
			if 'uuid' in a[0]:
				beam_uuid = a[1]

		print('Set beam uuid, beam_uuid={}'.format(beam_uuid))

	@validate_args(options=['id', 'token'])
	def do_set_user(self, arg):
		''' Set the user id and token for which all subsequent commands reference \
			User must be registered and/or logged in before using this command \
			[args: <id> and <token>]'''

		global user_id, user_token
		argmap = parse(arg)
		for a in argmap:
			if 'id' in a[0]:
				user_id = int(a[1])
			if 'token' in a[0]:
				user_token = a[1]

		print('Set user, user_id={}, user_token={}'.format(user_id, user_token))

	def do_quit(self, arg):
		''' Exit the Beam cli '''
		global mqtt_client
		mqtt_client.stop()
		print('Good bye')
		sys.exit()

def process_arg(arg):
	if '=' in arg:
		return arg.split('=')
	else:
		return arg

def parse(arg):
	'Convert a series of zero or more numbers to an argument tuple'
	return tuple(map(process_arg, arg.split()))

if __name__ == '__main__':
	mqtt_client = MqttClient()
	mqtt_client.start()
	# on mqtt broker connected, call scan api
	BeamShell().cmdloop()
